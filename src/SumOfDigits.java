import java.util.Scanner;

public class SumOfDigits {
    public static void main(String[] args) {
        System.out.print("Enter a three-digit integer: ");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();

        int sumOfDigits = number / 100 + (number % 100) / 10 + number % 10;

        System.out.println("Sum of all digits in 565 is " + sumOfDigits);
        /*
        Залишок від ділення (оператор mod) заданого числа на 10 дасть нам останню цифру числа.
        Залишок від ділення (оператор mod) заданого числа на 100 дасть нам останні дві цифри числа.
        Після цього ділимо отримане двоцифрове число націло на 10 - знайдемо другу цифру числа.
        Якщо задане трицифрове число розділити націло на 100, то вийде перша цифра числа.
        */
    }
}
